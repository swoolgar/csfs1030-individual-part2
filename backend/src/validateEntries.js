const validEntry = (req, res, next) => {
  let errMsg = {
    message: "validation error",
    invalid: [],
  };

  //valid name
  if (!req.body.name) {
    errMsg.invalid.push("name");
  } else if (typeof req.body.name != "string") {
    errMsg.invalid.push("name");
  }

  //valid email
  if (!emailValidate(req.body.email)) {
    errMsg.invalid.push("email");
  }

  //valid phone number
  if (!req.body.phoneNumber) {
    errMsg.invalid.push("phone number");
  } else if (req.body.phoneNumber.length < 8) {
    errMsg.invalid.push("phone number");
  }

  if (!req.body.content) {
    errMsg.invalid.push("content");
  }

  if (errMsg.invalid.length > 0) {
    return res.status(400).json(errMsg);
  }
  next();
};

export { validEntry };
