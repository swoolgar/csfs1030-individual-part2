import express from 'express';
import * as resumedb from '../database/resumePageDatabase';

const router = express.Router ();

//add work experience
router.post('/add-work', async (req, res) => {
    try {
        const newWork = { ... req.body,};
        await resumedb.newWorkExperience(newWork);
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' });
    }
})

//update work experience
router.put('/upd-work', async (req, res) => {
    try {
        const { jobTitle, employer, startDate, endDate, jobDetails } = req.body;
        await resumedb.updateWorkExperience([jobTitle, employer, startDate, endDate, jobDetails]);
        return res.send('work experience has been updated');
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' });
    }
})

//delete work experience
router.delete('/delete-work/:jobTitle', async (req, res) => {
    try {
        const delWork = req.params.jobTitle;
        await resumedb.deleteWorkExperience(delWork);
        return res.send('work experience deleted');
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' })
    }
})


//add education
router.post('/add-education', async (req, res) => {
    try {
        const newEdu = { ... req.body,};
        await resumedb.newEducation(newEdu);
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' });
    }
})

//update education
router.put('/upd-edu', async (req, res) => {
    try {
        const { schoolName, gradDate, programName } = req.body;
        await resumedb.updateEducation([schoolName, gradDate, programName]);
        return res.send('education has been updated');
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' });
    }
})

//delete education
router.delete('/delete-edu/:programName', async (req, res) => {
    try {
        const delEdu = req.params.jobTitle;
        await resumedb.deleteEducation(delWork);
        return res.send('education deleted');
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' })
    }
})

//add qualifications
router.post('/add-qualification', async (req, res) => {
    try {
        const newQualif = { ... req.body,};
        await resumedb.newQualification(newQualif);
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' });
    }
})

//update qualifications
router.put('/upd-qualification', async (req, res) => {
    try {
        const { qualificationID, qualificationDesc } = req.body;
        await resumedb.updateQualification([qualificationID, qualificationDesc]);
        return res.send('qualification has been updated');
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' });
    }
})

//delete qualifications
router.delete('/delete-qualification/:qualificationID', async (req, res) => {
    try {
        const delQualif = req.params.jobTitle;
        await resumedb.deleteQualification(delQualif);
        return res.send('qualification deleted');
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' })
    }
})

//add additional experience
router.post('/add-exp', async (req, res) => {
    try {
        const newExp = { ... req.body,};
        await resumedb.newAdditionalExp(newExp);
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' });
    }
})

//update additional experience
router.put('/upd-exp', async (req, res) => {
    try {
        const { experienceID, experienceDesc } = req.body;
        await resumedb.updateAdditionalExp([experienceID, experienceDesc]);
        return res.send('qualification has been updated');
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' });
    }
})

//delete additional experience
router.delete('/delete-exp/:qualificationID', async (req, res) => {
    try {
        const delQualif = req.params.jobTitle;
        await resumedb.deleteQualification(delQualif);
        return res.send('qualification deleted');
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'server error' })
    }
})


export default router;