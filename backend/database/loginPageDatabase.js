import { poolConnection } from "./databaseConnection";

const adminLogin = async (data) => {
    const result = await poolConnection(async (connection) => {
      const rows = await new Promise((resolve, reject) => {
        connection.query(
          "SELECT * FROM personalsite.adminuser  WHERE adminUsername = ? ",
          data,
          (err, rows) => {
            if (err) {
              reject(err);
            } else {
              resolve(rows);
            }
          }
        );
      });
      return rows;
    });
    return result;
  };

  export { adminLogin }