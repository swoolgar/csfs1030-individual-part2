import React from "react";
import { Redirect, Route } from "react-router-dom";

const parseJwt = (token) => {
    try {
      return JSON.parse(atob(token.split(".")[1]));
    } catch (e) {
      return null;
    }
  };
  
  const isAuthenticated = () => {
    try {
      return parseJwt(sessionStorage.getItem("token"));
    } catch (error) {
      console.log(error);
      return false;
    }
  };

function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{ pathname: "/admin-login", state: { from: location } }}
          />
        )
      }
    />
  );
}
export default PrivateRoute;
